package app.gradeapp;

import java.util.List;

/*
 * Object class for converting JSON string.
 */

public class Deliverable {

    /*
     * The List, and Attributes must be named the same as in the JSON string.
     */
    private List<Deliverable> deliverables;
    private String name;
    private double weight;

    //standard constructors
    public Deliverable(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    public Deliverable() {
    }

    //standard getters and setters
    public List<Deliverable> getDeliverables() {
        return deliverables;
    }

    public void setDeliverables(List<Deliverable> deliverables) {
        this.deliverables = deliverables;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Name: " + name
                +"\nWeight: " + weight;
    }
}
