package info.grade.util;

import info.grade.model.Deliverables;
import java.util.ArrayList;

public class GradesUtil {

    public static String[] calculateGarde(ArrayList<Deliverables> deliverables, String[] userData) {
        //create an array to hold grade information 
        String[] results = null;

        if (!deliverables.isEmpty() && userData.length > 0) {
            int x = 0;
            int size = deliverables.size();
            double[] grades = new double[size];
            results = new String[(size + 1)];
            for (Deliverables info : deliverables) {
                double percentage = Double.parseDouble(userData[x]);
                double weight = info.getWeight();
                double grade = ((percentage * weight) / 100);
                if (grade >= 0 && grade <= weight) {
                    grades[x] = grade;
                    results[x] = Double.toString(grade);
                }
                x++;
            }
            if (grades != null) {
                int length = grades.length;
                results[length] = Double.toString(getAverage(grades));
                return results;
            }
        }
        return results;
    }

    /*
     * Gets average using grades
     *
     * @author Kody D.
     */
    public static double getAverage(double[] grades) {
        double average = 0;
        int length = grades.length;
        for (int i = 0; i < length; i++) {
            average += grades[i];
        }
        return Math.round(average);
    }
}
