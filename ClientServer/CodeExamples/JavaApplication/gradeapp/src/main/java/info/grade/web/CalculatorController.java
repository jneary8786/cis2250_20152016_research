package info.grade.web;

import info.grade.model.Deliverables;
import info.grade.service.GradesService;
import info.grade.util.GradesUtil;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Purpose: Directs server to correct page and adds needed information to each
 * page.
 */
@Controller
public class CalculatorController {

    private final GradesService service;

    @Autowired
    public CalculatorController(GradesService service) {
        this.service = service;
    }

    @RequestMapping("/addDeliverable")
    public String showAddDeliverable(@ModelAttribute("deliverable") Deliverables deliverable, Model model, HttpSession session) {
        return "calculator/addDeliverable";
    }

    @RequestMapping("/add")
    public String add(@Valid @ModelAttribute("deliverable") Deliverables deliverable, BindingResult results, Model model, HttpSession session) {
        if (!results.hasErrors()) {
            String message = service.saveDeliverable(deliverable);
            model.addAttribute("message", message);
        }
        return "calculator/addDeliverable";
    }

    @RequestMapping("/deliverables")
    public String showDeliverables(Model model, HttpSession session) {
        model.addAttribute("deliverables", service.getDeliverables());
        return "calculator/deliverables";
    }

    @RequestMapping("/delete")
    public String deleteDeliverable(@RequestParam("id") int id, Model model, HttpSession session) {
        if (id > 0) {
            model.addAttribute("message", service.deleteDeliverable(id));
        }
        model.addAttribute("deliverables", service.getDeliverables());
        return "calculator/deliverables";
    }

    @RequestMapping("/calculate")
    public String calculate(@RequestParam(value = "grade") String[] grades, Model model, HttpSession session) {
        if (grades != null) {
            String[] results = GradesUtil.calculateGarde(service.getDeliverables(), grades);
            int length = results.length;
            model.addAttribute("grades", results);
            model.addAttribute("mark", results[(length - 1)]);
        }
        model.addAttribute("deliverables", service.getDeliverables());
        return "calculator/deliverables";
    }
}
