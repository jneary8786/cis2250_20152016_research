package info.grade.service;

import info.grade.dao.DeliverablesDAO;
import info.grade.data.jparepository.DeliverablesRepository;
import info.grade.model.Deliverables;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Purpose: Implementation class for Court Service which forces all need methods
 * to be create for a successful deployment of application. Also is used as a
 * facade for all controllers;
 */
@Service
public class GradesServiceImpl implements GradesService {

    private final DeliverablesRepository deliverablesRepository;

    @Autowired
    public GradesServiceImpl(DeliverablesRepository deliverablesRepository) {
        this.deliverablesRepository = deliverablesRepository;
    }

    @Override
    public String saveDeliverable(Deliverables deliverable) {
        System.out.println("ADDING: Deliverable To Database."
                + "FROM: GradesServiceImpl");
        String message;
        try {
            double maxWeight = 100;
            double totalWeight = DeliverablesDAO.getTotalWeight();

            if (totalWeight == maxWeight) {
                message = "Weight at 100.";
            } else {
                double weight = maxWeight - totalWeight;
                if (deliverable.getWeight() <= weight) {
                    deliverablesRepository.save(deliverable);
                    message = "Deliverable was added.";
                } else {
                    message = "Weight must be " + weight + " or less.";
                }
            }
        } catch (Exception ex) {
            message = "Deliverable wasn't added.";
            System.out.println("HIBERNATE SQL INSERT ERROR: " + ex.toString()
                    + "\nWHERE: " + ex.getClass());
        }
        return message;
    }

    @Override
    public String deleteDeliverable(int id) {
        System.out.println("DELETING: Deliverable To Database."
                + "FROM: GradesServiceImpl");
        String message;
        try {
            deliverablesRepository.delete(id);
            message = "Deliverable was removed.";
        } catch (Exception ex) {
            message = "Deliverable wasn't removed.";
            System.out.println("HIBERNATE SQL INSERT ERROR: " + ex.toString()
                    + "\nWHERE: " + ex.getClass());
        }
        return message;
    }

    @Override
    public ArrayList<Deliverables> getDeliverables() {
        System.out.println("RETURNING: Deliverables To Database."
                + "FROM: GradesServiceImpl");
        return deliverablesRepository.findAll();
    }
}
